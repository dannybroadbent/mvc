<?php
class UsersModel extends database {
	public function UsersModel() {
		$this->DatabaseConn();
		
		// define table name and primary key
		$this->SetTableName('users');
		$this->AutoFields('users');
		$this->SetPrimaryKey('id');
		
		// define default order fields
		$this->defaultOrderFields['id'] = 'ASC';
	}
	
	public function addUser($fields) {
		$addUser = clone $this;
		$chkUser = clone $this;
		$chkUser->AddWhere('AND fbid LIKE "'.$fields['fbid'].'"');
		if (!$chkUser->LoadRecords()) {
			foreach ($fields as $field => $value) {
				$addUser->SetField($field, $value);
			}
			$addUser->SaveRecord();
		}
	}
	
	public function getUsers($exclude_id) {
		$users = array();
		$chkUser = clone $this;
		$getUsers = clone $this;
		$chkUser->AddWhere('AND fbid NOT LIKE "'.$exclude_id.'"');
		if ($chkUser->LoadRecords()) {
			while ($chkUser->read()) {
				$users[] = $chkUser->f('name');
			}
		}
		return $users;
	}
}