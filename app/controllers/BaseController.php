<?php
class BaseController {
	public function loadModel($table) {
		require_once(SYS_PATH.'/app/models/'.$table.'.php');
		$model = '$model = new '.ucwords($table).'Model();';
		eval($model);
		return $model;
	}
	
	public function render($file, $data = array()) {
		if (file_exists(SYS_PATH.'/app/'.$file.'.php')) {
			extract($data);
			ob_start();
			require(SYS_PATH.'/app/'.$file.'.php');
			$output = ob_get_contents();
			ob_end_clean();
			echo $output;
		} else {
			echo 'Failed to load view: '.SYS_PATH.'/app/'.$file.'.php!';
			exit;
		}
	}
}