<?php
class FacebookController extends BaseController {
	private $facebook = NULL;
	
	public function __construct() {
		global $fbconfig;
		$this->facebook = new Facebook($fbconfig);
	}
	
	public function login() {
		$user_id = $this->facebook->getUser();
		if ((int)$user_id>0) {
			header('Location: /facebook/view');
			exit;
		} else {
			$data['login_url'] = $this->facebook->getLoginUrl();
			$this->render('views/facebook/login', $data);
		}
	}
	
	public function view() {
		$user_id = $this->facebook->getUser();
		try {
			$data['user_profile'] = $this->facebook->api('/me','GET');
		} catch(FacebookApiException $e) {
			header('Location: /facebook/login');
			exit;
		}
		
		$fbid = $data['user_profile']['id'];
		
		$users = $this->loadModel('users');
		$fields = array(
			'fbid' => $fbid,
			'name' => (string)$data['user_profile']['name']
		);
		$users->addUser($fields);
		$data['users'] = $users->getUsers($fbid);
		
		$this->render('views/facebook/view', $data);
	}
}