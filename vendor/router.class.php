<?php
class Router {
	public function load() {
		$urlPass = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);
		
		$chkURL = explode('/', $urlPass);
		
		$file = $chkURL[count($chkURL)-2];
		$method = $chkURL[count($chkURL)-1];
		$path = str_replace('/'.$file, '', str_replace('/'.$method, '', $urlPass));
		
		if ($chkURL[1] != '') {
			if (file_exists(SYS_PATH.'/app/controllers'.$path.'/'.ucwords($file).'Controller.php')) {
				http_response_code(200);
				require_once(SYS_PATH.'/app/controllers'.$path.'/'.ucwords($file).'Controller.php');
				$loadClass = '$load = new '.ucwords($file).'Controller;';
				$loadClass.= '$load->'.strtolower($method).'();';
				eval($loadClass);
			} else {
				http_response_code(404);
				require_once(SYS_PATH.'/app/controllers/ErrorController.php');
				$load = new ErrorController;
				$load->view();
			}
		} else if ($chkURL[1] == '') {
			http_response_code(200);
			require_once(SYS_PATH.'/app/controllers/FrontController.php');
			$load = new FrontController;
			$load->view();
		}
	}
}